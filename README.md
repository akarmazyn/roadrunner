Roadrunner
============================

Tool to measure frontend performance.
This tool is based on Silex - Kitchen Sink Edition

For more informations, see the
[**dedicated page**](http://lyrixx.github.com/Silex-Kitchen-Edition).
