<?php

namespace Runner;

use \Silex\Application,
    \Silex\ServiceProviderInterface;

class ServiceProvider implements ServiceProviderInterface {
    /**
     * Bootstraps the application.
     *
     * This method is called after all services are registers
     * and should be used for "dynamic" configuration (whenever
     * a service must be requested).
     */
    public function boot (Application $app) {
        return $app;
    }

    /**
     * Registers services on the given app.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     *
     * @param Application $app An Application instance
     */
    public function register (Application $app) {
        $app['runner'] = $app->share(function ($app) {
            return new Manager($app);
        });
    }
}
