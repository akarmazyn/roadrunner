<?php

namespace Runner;


class Manager {
    const TYPE_TOKEN_REQUEST  = 'request token';
    const TYPE_MEASURE_FPS    = 'fps';
    const TYPE_MEASURE_MEMORY = 'memory';
    const TYPE_NAVIGATION     = 'navigation';
    const TYPE_TIME           = 'time';

    const RESULT_LIMIT = 20;

    protected $app, $runner, $project;

    public function __construct($app) {
        $this->app = $app;
    }

    /**
     * Sets current project
     *
     * @param string $project Project name
     */
    public function setProject($project) {
        $this->project = $project;
    }

    public function process($runner) {
        $this->runner = $runner;
        if (!isset($runner['type'])) {
            return $this->badTypeResponse();
        }
        switch ($runner['type']) {
            case self::TYPE_TOKEN_REQUEST:
                return $this->tokenRequest();

            case self::TYPE_MEASURE_FPS:
                return $this->fps();

            case self::TYPE_MEASURE_MEMORY:
                return $this->memory();

            case self::TYPE_NAVIGATION:
                return $this->navigation();

            case self::TYPE_TIME:
                return $this->time();

            default:
                return $this->badTypeResponse();
        }
    }

    /**
     * Initializes session, creates token for runner
     *
     * @return array
     */
    protected function tokenRequest() {
        $token = $this->createToken();
        $this->runner['token'] = $token;

        /** @var \Doctrine\MongoDB\Database $database */
        $database = $this->app['db'];
        $database->selectCollection('session')->insert($this->runner);

        return array(
            'token' => $token,
            'code' => 201 // created
        );
    }

    /**
     * Saves information about runner fps
     *
     * @return array
     */
    protected function fps() {
        /** @var \Doctrine\MongoDB\Database $database */
        $database = $this->app['db'];
        $database->selectCollection('fps')->insert($this->runner);

        return array(
            'code' => 201
        );
    }

    /**
     * Saves information about runner memory
     *
     * @return array
     */
    protected function memory() {
        /** @var \Doctrine\MongoDB\Database $database */
        $database = $this->app['db'];
        $database->selectCollection('memory')->insert($this->runner);

        return array(
            'code' => 201
        );
    }

    /**
     * Saves information about runner navigation
     *
     * @return array
     */
    protected function navigation() {
        /** @var \Doctrine\MongoDB\Database $database */
        $database = $this->app['db'];
        $database->selectCollection('navigation')->insert($this->runner);

        return array(
            'code' => 201
        );
    }

    /**
     * Saves information about runner time
     *
     * @return array
     */
    protected function time() {
        /** @var \Doctrine\MongoDB\Database $database */
        $database = $this->app['db'];
        $database->selectCollection('time')->insert($this->runner);

        return array(
            'code' => 201
        );
    }

    /**
     * Returns information about sessions
     *
     * @return array
     */
    public function getSessions() {
        $sort = array(
            'timestamp' => -1
        );
        /** @var \Doctrine\MongoDB\Database $database */
        $database = $this->app['db'];
        return $database->selectCollection('session')->find()->sort($sort)->toArray(false);
    }

    /**
     * Returns information about fps
     *
     * @param null|string $token Runner token
     * @param array       $fields Returned json fields
     * @return array
     */
    public function getFps($token = null, $fields = array()) {
        $query = array();
        $sort = array(
            'timestamp' => -1
        );
        /** @var \Doctrine\MongoDB\Database $database */
        $database = $this->app['db'];
        $collection = $database->selectCollection('fps');
        if (!empty($token)) {
            $query = array(
                'token' => $token
            );
        }
        if ($this->project) {
            $query = array(
                'token' => array(
                    '$in' => $this->getProjectTokens()
                )
            );
        }

        return $collection->find($query, $fields)
            ->limit(self::RESULT_LIMIT)
            ->sort($sort)
            ->toArray(false);
    }

    /**
     * Returns tokens for given project
     *
     * @return array
     */
    protected function getProjectTokens() {
        $sessions = $this->app['db']->selectCollection('session')
            ->find(array('data.project' => $this->project))
            ->toArray();
        $tokens = array();

        foreach ($sessions as $session) {
            $tokens[] = $session['token'];
        }
        return $tokens;
    }

    /**
     * Returns information about memory
     *
     * @param null|string $token Runner token
     * @return array
     */
    public function getMemory($token = null) {
        $query = array();
        $sort = array(
            'timestamp' => -1
        );
        /** @var \Doctrine\MongoDB\Database $database */
        $database = $this->app['db'];
        $collection = $database->selectCollection('memory');
        if (!empty($token)) {
            $query = array(
                'token' => $token
            );
        }

        if ($this->project) {
            $query = array(
                'token' => array(
                    '$in' => $this->getProjectTokens()
                )
            );
        }

        return $collection->find($query)
            ->limit(self::RESULT_LIMIT)
            ->sort($sort)
            ->toArray(false);
    }

    /**
     * Returns information about navigation
     *
     * @param null|string $token Runner token
     * @return array
     */
    public function getNavigation($token = null) {
        $query = array();
        $sort = array(
            'timestamp' => -1
        );
        /** @var \Doctrine\MongoDB\Database $database */
        $database = $this->app['db'];
        $collection = $database->selectCollection('navigation');
        if (!empty($token)) {
            $query = array(
                'token' => $token
            );
        }

        if ($this->project) {
            $query = array(
                'token' => array(
                    '$in' => $this->getProjectTokens()
                )
            );
        }

        return $collection->find($query)
            ->limit(self::RESULT_LIMIT)
            ->sort($sort)
            ->toArray(false);
    }

    /**
     * Returns information about time
     *
     * @param null|string $token Runner token
     * @return array
     */
    public function getTime($token = null) {
        $query = array();
        $sort = array(
            'timestamp' => -1
        );
        /** @var \Doctrine\MongoDB\Database $database */
        $database = $this->app['db'];
        $collection = $database->selectCollection('time');
        if (!empty($token)) {
            $query = array(
                'token' => $token
            );
        }

        if ($this->project) {
            $query = array(
                'token' => array(
                    '$in' => $this->getProjectTokens()
                )
            );
        }

        return $collection->find($query)
            ->limit(self::RESULT_LIMIT)
            ->sort($sort)
            ->toArray(false);
    }

    /**
     * Creates unique token for runner session
     *
     * @return string
     */
    protected function createToken() {
        return sha1(uniqid('', true));
    }

    /**
     * Returns response for bad runner request
     *
     * @return array
     */
    protected function badTypeResponse() {
        return array(
            'error' => sprintf('Type can be one of: %s', implode(', ', $this->getTypes())),
            'code'  => 400 // bad request
        );
    }

    /**
     * Returns valid runner types
     *
     * @return array
     */
    protected function getTypes() {
        return array(
            self::TYPE_TOKEN_REQUEST,
            self::TYPE_MEASURE_FPS,
            self::TYPE_MEASURE_MEMORY,
            self::TYPE_NAVIGATION,
            self::TYPE_TIME
        );
    }
}
