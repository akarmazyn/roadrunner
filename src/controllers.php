<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

$app->before(function (Request $request) {
    if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
        $data = json_decode($request->getContent(), true);
        $request->request->replace(is_array($data) ? $data : array());
    }
});
//
$app->match('/api/run', function() use ($app) {
    $headers = array(
        'Access-Control-Allow-Origin' => '*',
        'Access-Control-Allow-Methods' => 'POST',
        'Access-Control-Allow-Headers' => 'origin, content-type, accept'
    );
    return new Response('', 200, $headers);
})->method('OPTIONS');

$app->post('/api/run', function(Request $request) use ($app) {
    /** @var Runner\Manager $runnerManager */
    $runnerManager = $app['runner'];
    $runner = array(
        'token' => $request->get('token'),
        'data' => $request->get('data'),
        'type' => $request->get('type'),
        'timestamp' => $request->get('timestamp')
    );

    $result = $runnerManager->process($runner);

    $headers = array(
        'Access-Control-Allow-Origin' => '*'
    );

    return $app->json($result, $result['code'], $headers);
});

$app->match('/', function() use ($app) {
    return $app['twig']->render('index.html.twig');
})->bind('homepage');

$app->match('/sessions', function() use ($app) {
    return $app['twig']->render('sessions.html.twig',
            array('sessions' => $app['runner']->getSessions())
    );
})->bind('sessions');

$app->match('/sessions/{token}', function($token) use ($app) {
    return $app['twig']->render('session_details.html.twig',
        array(
            'fpss' => $app['runner']->getFps($token),
            'memorys' => $app['runner']->getMemory($token),
            'navigations' => $app['runner']->getNavigation($token),
            'times' => $app['runner']->getTime($token),
            'token' => $token
        )
    );
})->bind('session_details');

$app->match('/project/{project}', function($project) use ($app) {
    $app['runner']->setProject($project);
    return $app['twig']->render('project_details.html.twig',
        array(
            'fpss' => $app['runner']->getFps(),
            'memorys' => $app['runner']->getMemory(),
            'navigations' => $app['runner']->getNavigation(),
            'times' => $app['runner']->getTime(),
            'project' => $project
        )
    );
})->bind('project_details');

$app->match('/fps', function() use ($app) {
    return $app['twig']->render('fps.html.twig',
            array('fpss' => $app['runner']->getFps())
    );
})->bind('fps');

$app->match('/fps/{token}', function($token) use ($app) {
    return $app['twig']->render('fps_session.html.twig',
            array(
                'fpss' => $app['runner']->getFps($token),
                'token' => $token
            )
    );
})->bind('fps_session');

$app->match('/memory', function() use ($app) {
    return $app['twig']->render('memory.html.twig',
            array('memorys' => $app['runner']->getMemory())
    );
})->bind('memory');

$app->match('/memory/{token}', function( $token) use ($app) {
    return $app['twig']->render('memory.html.twig',
            array('memorys' => $app['runner']->getMemory($token))
    );
})->bind('memory_session');

$app->match('/navigation', function() use ($app) {
    return $app['twig']->render('navigation.html.twig',
            array('navigations' => $app['runner']->getNavigation())
    );
})->bind('navigation');

$app->match('/navigation/{token}', function($token) use ($app) {
    return $app['twig']->render('navigation.html.twig',
        array('navigations' => $app['runner']->getNavigation($token))
    );
})->bind('navigation_session');

$app->match('/time', function() use ($app) {
    return $app['twig']->render('time.html.twig',
            array('times' => $app['runner']->getTime())
    );
})->bind('time');

$app->match('/time/{token}', function($token) use ($app) {
    return $app['twig']->render('time.html.twig',
        array('times' => $app['runner']->getTime($token))
    );
})->bind('time_session');

$app->match('/api/fps/{token}', function($token) use ($app) {
    $fps = $app['runner']->getFps($token, array('timestamp', 'data'));

    return $app->json($fps);
})->bind('api_fps');

$app->match('/api/fps/p/{project}', function($project) use ($app) {
    $app['runner']->setProject($project);
    $fps = $app['runner']->getFps(null, array('timestamp', 'data'));

    return $app->json($fps);
})->bind('api_fps_project');

$app->error(function (\Exception $e, $code) use ($app) {
    if ($app['debug']) {
        return;
    }

    $app['logger']->err($e->getMessage());

    switch ($code) {
        case 404:
            $message = 'The requested page could not be found.';
            break;
        default:
            $message = 'We are sorry, but something went terribly wrong.';
    }

    return new Response($message, $code);
});

return $app;
